//
//  EditorVC.m
//  Snoopify
//
//  Created by MAC on 8/27/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#define kImageControlerTag  200

#define controlWidth2  220
#define controlHeight2  220
#import "EditorVC.h"

@implementation EditorVC

-(void)setEditorModeOn:(BOOL)editorModeOn {
    _editorModeOn = editorModeOn;
    self.controlEditPanelView.hidden=!_editorModeOn;
    self.stickerAddButton.hidden=_editorModeOn;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    imageTag=kImageControlerTag;
    self.imageViewArray=[NSMutableArray new];
    self.navigationController.navigationBarHidden=YES;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"editorBG"]];
    self.ediorImageView.image=self.originalImage;
    [self prefersStatusBarHidden];
    self.editorModeOn=NO;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.view sendSubviewToBack:self.containerView];
}
-(void)selectImage:(UIImage*)image{
    self.editorModeOn=YES;
    CGFloat viewHight=(([[UIScreen mainScreen] bounds]).size.height);
    CGFloat viewWidth=(([[UIScreen mainScreen] bounds]).size.width);
    viewControl=[[ViewControl alloc] initWithFrame:CGRectMake((viewWidth-controlWidth2)/2, (viewHight-controlHeight2)/2-100, controlWidth2, controlHeight2) controlXPosition:10 controlYPositoin:10 controlWidth1:controlWidth2-20 controlHeight1:controlHeight2-20 image:image];
    
    UITapGestureRecognizer *tapRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnControl:)];
    [viewControl addGestureRecognizer:tapRecognizer1];
    viewControl.tag=[self.imageViewArray count];
    
    [self.containerView addSubview:viewControl];
    [self.imageViewArray addObject:viewControl];
}
-(void)selectImage1:(UIImage*)image{
    self.editorModeOn=YES;
    controlView=[[ViewClass alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, self.view.frame.size.height-150) controlXPosition:30 controlYPositoin:50 controlWidth1:220 controlHeight1:200 image:image];
    
    UITapGestureRecognizer *tapRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnControl:)];
    [controlView addGestureRecognizer:tapRecognizer1];
    controlView.tag=[self.imageViewArray count];
    [self.containerView addSubview:controlView];
    [self.imageViewArray addObject:controlView];
}
- (void)tapOnControl:(UITapGestureRecognizer *)sender
{
    for (ViewControl *imageView1 in self.imageViewArray) {
        [imageView1 deselectController];
    }
    ViewControl *imageView=(ViewControl*)sender.view;
    [imageView selectContollerWith:imageView];
    self.editorModeOn=YES;
    [self.containerView bringSubviewToFront:imageView];
}
-(IBAction)doneButtonAction:(id)sender{
    for (ViewControl *imageView1 in self.imageViewArray) {
        [imageView1 deselectController];
    }
    self.editorModeOn=NO;
}
-(IBAction)clearButtonAction:(id)sender{
    for (ViewControl *imageView1 in self.imageViewArray) {
        [imageView1 removeFromSuperview];
    }
    [self.imageViewArray removeAllObjects];
    self.editorModeOn=NO;
}
/*
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint locationPoint;// = [[touches anyObject] locationInView:self.view];
    NSArray *subviews = [self.containerView subviews];
    // Return if there are no subviews
    if ([subviews count] == 0) return;
    UIView* subview;
    for (NSInteger i=[subviews count]-1;i>=0;i--) {
        subview=[subviews objectAtIndex:i];
        //for (subview in subviews){
        
        ViewClass *myImageView = (ViewClass *)[self.containerView viewWithTag:subview.tag];
        NSLog(@"found image");
        locationPoint = [[touches anyObject] locationInView:myImageView.mainControllerView];
        if (CGRectContainsPoint(myImageView.borderView.frame, locationPoint)) {
            NSLog(@"found image finally");
            //[myImageView becomeFirstResponder];
            [myImageView selectContollerWith:myImageView];
            return;
        }
    }
}
*/
-(IBAction)addSticker:(id)sender{
    [self performSegueWithIdentifier:@"AddStickerSegue" sender:self];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"AddStickerSegue"]) {
        StickerVC *stickerVC=[segue destinationViewController];
        stickerVC.delegate=self;
    }
}
-(IBAction)newButtonAction:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:NO];
}
@end