//
//  ViewClass.m
//  ResizingApp
//
//  Created by  Dulal Hossain on 8/12/12.
//  Copyright (c) 2012 Databiz Software Ltd. All rights reserved.
//

#import "ViewClass.h"

@implementation ViewClass
@synthesize imageView,mainControllerView,borderView;

- (id)initWithFrame:(CGRect)frame controlXPosition:(NSInteger)xPositon controlYPositoin:(NSInteger)yPosition controlWidth1:(NSInteger)width controlHeight1:(NSInteger)height image:(UIImage *)image
{
    self = [super initWithFrame:frame];
    if (self) {
        
        controlXPossition=xPositon;
        controlYPossition=yPosition;
        controlWidth=width;
        controlHeight=height;
        self.image=image;
        
        self.clipsToBounds=YES;
   
        [self initController];
       //// self.rotation = [[RotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotating:)];
        dragRecognizer=[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetectedOnController:)];
       
        borderPinchGR=[[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchDetectedOnBorderView:)];
        borderPinchGR.delegate=self;
        borderRotationGR=[[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotationDetectedOnBorderView:)];
        borderRotationGR.delegate=self;
        [self unLockControlView];
    }
    return self;
}

-(void)initController
{
    // Initialization code
    self.backgroundColor=[UIColor clearColor];
    self.userInteractionEnabled=YES;
    self.layer.borderWidth = 3.0;
    self.layer.borderColor=[[UIColor colorWithRed:0 green:0 blue:0 alpha:0.0] CGColor];

    //self.clipsToBounds=YES;
    
  ///  mainControllerView=[[UIView alloc]initWithFrame:CGRectMake(-self.frame.size.width, -self.frame.size.height, self.frame.size.width*3, self.frame.size.height*3)];
    
    mainControllerView=[[UIView alloc]initWithFrame:CGRectMake(0,0, self.frame.size.width, self.frame.size.height)];
    mainControllerView.backgroundColor=[UIColor clearColor];
    mainControllerView.userInteractionEnabled=YES;
    [self addSubview:mainControllerView];
    
   /// imageView=[[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width+controlXPossition, self.frame.size.height+controlYPossition, controlWidth, controlHeight)];
     imageView=[[UIImageView alloc] initWithFrame:CGRectMake(controlXPossition, controlYPossition, controlWidth, controlHeight)];
    imageView.backgroundColor=[UIColor clearColor];
    imageView.image=self.image;
    imageView.userInteractionEnabled=YES;
   
    [mainControllerView addSubview:imageView];
    
    ///borderView=[[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width+controlXPossition, self.frame.size.height+controlYPossition, controlWidth, controlHeight)];
    borderView=[[UIView alloc] initWithFrame:CGRectMake(controlXPossition, controlYPossition, controlWidth, controlHeight)];
    borderView.userInteractionEnabled=YES;
    borderView.layer.borderWidth = 3.0;
    borderView.backgroundColor=[UIColor clearColor];
    borderView.layer.borderColor=[[UIColor colorWithRed:235.0/255 green:0.0/255 blue:139.0/255 alpha:1.0] CGColor];

    [mainControllerView addSubview:borderView];
    
   // [rotation setOtherView:borderView];
}

-(void)resetContoller
{
    [mainControllerView removeFromSuperview];
    [self initController];
}
/*
- (void)rotating:(RotationGestureRecognizer *)recognizer
{
    //UIView *view = [recognizer view];
    CGFloat angle = [recognizer rotation];
    
    NSLog(@"rotation %f",angle);
    
    if (angle<0.09) {
        angle*=1;
    }
    CGPoint point = [mainControllerView convertPoint :borderView.center toView:self];
    NSLog(@"%f %f",point.x, point.y);
    
    CGRect imageBounds = mainControllerView.bounds;
    [mainControllerView.layer setAnchorPoint:CGPointMake(borderView.center.x / imageBounds.size.width, borderView.center.y / imageBounds.size.height)];
    mainControllerView.layer.position=point;
    
    mainControllerView.transform = CGAffineTransformRotate(mainControllerView.transform, angle);
    
    recognizer.rotation=0.0;
}
*/
-(void)unLockControlView
{
    if (imageView.userInteractionEnabled==YES)
    {
       //// [rotation setOtherView:borderView];

        ////[mainControllerView addGestureRecognizer:rotation];
   
        imageView.userInteractionEnabled=NO;
        borderView.userInteractionEnabled=YES;
      
        [borderView addGestureRecognizer:dragRecognizer];
        [borderView addGestureRecognizer:borderPinchGR];
        [borderView addGestureRecognizer:borderRotationGR];
    }
    else
    {
       //// [mainControllerView removeGestureRecognizer:rotation];
        imageView.userInteractionEnabled=YES;
        borderView.userInteractionEnabled=NO;
        
        [borderView removeGestureRecognizer:dragRecognizer];
        [borderView removeGestureRecognizer:borderPinchGR];
        [borderView removeGestureRecognizer:borderRotationGR];
    }
}
- (void)removeControlView
{
    [mainControllerView removeFromSuperview];
    [self removeFromSuperview];
}
- (void)rotationDetectedOnBorderView:(UIRotationGestureRecognizer *)rotationRecognizer
{
    CGFloat angle = rotationRecognizer.rotation;
    
    CGPoint point = [mainControllerView convertPoint :borderView.center toView:self];
    NSLog(@"%f %f",point.x, point.y);
    
    CGPoint pointi = CGPointMake((borderView.frame.origin.x+borderView.frame.size.width/2), (borderView.frame.origin.y+borderView.frame.size.height/2));
    NSLog(@"%f %f",pointi.x, pointi.y);
    
    CGRect imageBounds = mainControllerView.bounds;
    [mainControllerView.layer setAnchorPoint:CGPointMake(borderView.center.x / imageBounds.size.width, borderView.center.y / imageBounds.size.height)];
    mainControllerView.layer.position=point;
    mainControllerView.transform = CGAffineTransformRotate(mainControllerView.transform, angle);
    rotationRecognizer.rotation = 0.0;
}

- (void)pinchDetectedOnBorderView:(UIPinchGestureRecognizer *)pinchRecognizer
{    
    CGFloat scale = pinchRecognizer.scale;
    UIView* view= [[UIView alloc] initWithFrame:borderView.frame] ;
    view.transform = CGAffineTransformScale(view.transform, scale, scale);
   /// [view setFrame:CGRectMake(0, 0, view.frame.size.width+80, view.frame.size.width+80)];
    view.center=borderView.center;
    
    if (!CGRectContainsRect(mainControllerView.frame, view.frame)) {
        return;
    }
    ////
    mainControllerView.transform = CGAffineTransformScale(mainControllerView.transform, scale, scale);
    borderView.transform = CGAffineTransformScale(borderView.transform, scale, scale);
    imageView.transform = CGAffineTransformScale(imageView.transform, scale, scale);
    pinchRecognizer.scale = 1.0;
}
- (void)panDetectedOnController:(UIPanGestureRecognizer *)panRecognizer
{
    CGPoint translation = [panRecognizer translationInView:self];
    CGPoint contollerPosition = mainControllerView.center;
    contollerPosition.x += translation.x;
    contollerPosition.y += translation.y;
    
    CGRect rect=borderView.frame;
    rect.origin.x+=translation.x;
    rect.origin.y+=translation.y;
    mainControllerView.center = contollerPosition;
    [panRecognizer setTranslation:CGPointZero inView:self];
}
-(void)cropImage
{
    CGRect rect = borderView.frame;
    borderView.layer.borderColor=[[UIColor colorWithRed:166.0/255 green:252.0/255 blue:255.0/255 alpha:0.0] CGColor];
    UIImage *image = [self captureView];
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], rect);
      borderView.layer.borderColor=[[UIColor colorWithRed:235.0/255 green:0.0/255 blue:139.0/255 alpha:1.0] CGColor];
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    imageView.image=img;
    imageView.transform = CGAffineTransformIdentity;
    [imageView setFrame:borderView.frame];
}
- (UIImage *)captureView{
    CGRect screenRect = [mainControllerView bounds];
    UIGraphicsBeginImageContext(screenRect.size);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [[UIColor clearColor] set];
    CGContextFillRect(ctx, screenRect);
    
    [mainControllerView.layer renderInContext:ctx];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return newImage;
}
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint locationPoint = [[touches anyObject] locationInView:self.mainControllerView];
    if (CGRectContainsPoint(borderView.frame,locationPoint)==NO)
    {
        [self deselectController];
    }
}

-(void) deselectController
{
    self.layer.borderColor=[[UIColor colorWithRed:0 green:0 blue:0 alpha:0.0] CGColor];
    borderView.layer.borderColor=[[UIColor colorWithRed:166.0/255 green:252.0/255 blue:255.0/255 alpha:0.0] CGColor];
    [self setUserInteractionEnabled:YES];
    [imageView setHidden:NO];
    [self resignFirstResponder];
}
-(void)selectContollerWith:(UIView *)view
{
    view.layer.borderColor=[[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0] CGColor];
    borderView.layer.borderColor=[[UIColor colorWithRed:235.0/255 green:0.0/255 blue:139.0/255 alpha:1.0] CGColor];
    [view setUserInteractionEnabled:YES];
    [view becomeFirstResponder];

    [imageView setHidden:NO];
}
@end