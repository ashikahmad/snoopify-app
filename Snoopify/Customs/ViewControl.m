//
//  ViewClass.m
//  ResizingApp
//
//  Created by  Dulal Hossain on 8/12/12.
//  Copyright (c) 2012 Databiz Software Ltd. All rights reserved.
//

#import "ViewControl.h"

@implementation ViewControl
@synthesize imageView,borderView;
-(void)setIsSelected:(BOOL)isSelected{
    _isSelected=isSelected;
    if (_isSelected) {
        //borderView.backgroundColor=[UIColor colorWithWhite:0.8 alpha:0.6];
       // borderView.layer.borderColor=[[UIColor colorWithRed:235.0/255 green:0.0/255 blue:139.0/255 alpha:1.0] CGColor];
    }
    else{
       // borderView.backgroundColor=[UIColor clearColor];
       // borderView.layer.borderColor=[[UIColor clearColor] CGColor];
    }
}
- (id)initWithFrame:(CGRect)frame controlXPosition:(NSInteger)xPositon controlYPositoin:(NSInteger)yPosition controlWidth1:(NSInteger)width controlHeight1:(NSInteger)height image:(UIImage *)image{
    self = [super initWithFrame:frame];
    if (self) {
        controlXPossition=xPositon;
        controlYPossition=yPosition;
        controlWidth=width;
        controlHeight=height;
        self.image=image;
        
        self.clipsToBounds=YES;
        [self initController];
        self.isSelected=YES;
        self.userInteractionEnabled=YES;
        
        dragRecognizer=[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetectedOnController:)];
       
        borderPinchGR=[[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchDetectedOnBorderView:)];
        borderPinchGR.delegate=self;
        borderRotationGR=[[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotationDetectedOnBorderView:)];
        borderRotationGR.delegate=self;
        
        [self addGestureRecognizer:dragRecognizer];
        [self addGestureRecognizer:borderPinchGR];
        [self addGestureRecognizer:borderRotationGR];
    }
    return self;
}
-(void)initController
{
    // Initialization code
    self.backgroundColor=[UIColor clearColor];
    self.layer.borderWidth = 3.0;
    self.layer.borderColor=[[UIColor colorWithRed:0 green:0 blue:0 alpha:0.0] CGColor];

    borderView=[[UIView alloc] initWithFrame:CGRectMake(controlXPossition, controlYPossition, controlWidth, controlHeight)];
    borderView.userInteractionEnabled=YES;
    borderView.layer.borderWidth = 3.0;
    borderView.backgroundColor=[UIColor colorWithWhite:1.0 alpha:0.3];
    borderView.layer.borderColor=[[UIColor colorWithRed:235.0/255 green:0.0/255 blue:139.0/255 alpha:1.0] CGColor];
    
    [self addSubview:borderView];
    
    imageView=[[UIImageView alloc] initWithFrame:CGRectMake(2, 2, borderView.frame.size.width-4, borderView.frame.size.height-4)];
    imageView.backgroundColor=[UIColor clearColor];
    imageView.image=self.image;
    imageView.userInteractionEnabled=YES;
    
    [borderView addSubview:imageView];
}
-(void)resetContoller
{
    [self removeFromSuperview];
    [self initController];
}
- (void)removeControlView
{
    [self removeFromSuperview];
    [self removeFromSuperview];
}
- (void)rotationDetectedOnBorderView:(UIRotationGestureRecognizer *)rotationRecognizer
{
    rotationRecognizer.view.transform = CGAffineTransformRotate(rotationRecognizer.view.transform, rotationRecognizer.rotation);
    rotationRecognizer.rotation = 0;
    /*
    CGFloat angle = rotationRecognizer.rotation;
    
    CGPoint point = [self.superview convertPoint :borderView.center toView:self];
    NSLog(@"%f %f",point.x, point.y);
    
    CGPoint pointi = CGPointMake((borderView.frame.origin.x+borderView.frame.size.width/2), (borderView.frame.origin.y+borderView.frame.size.height/2));
    NSLog(@"%f %f",pointi.x, pointi.y);
    
    CGRect imageBounds = self.superview.bounds;
    [self.superview.layer setAnchorPoint:CGPointMake(borderView.center.x / imageBounds.size.width, borderView.center.y / imageBounds.size.height)];
    self.layer.position=point;
    self.transform = CGAffineTransformRotate(self.transform, angle);
    rotationRecognizer.rotation = 0.0;*/
}

- (void)pinchDetectedOnBorderView:(UIPinchGestureRecognizer *)pinchRecognizer
{
    pinchRecognizer.view.transform = CGAffineTransformScale(pinchRecognizer.view.transform, pinchRecognizer.scale, pinchRecognizer.scale);
    pinchRecognizer.scale = 1;
    
    /*
    
    CGFloat scale = pinchRecognizer.scale;
    UIView* view= [[UIView alloc] initWithFrame:borderView.frame] ;
    view.transform = CGAffineTransformScale(view.transform, scale, scale);
   /// [view setFrame:CGRectMake(0, 0, view.frame.size.width+80, view.frame.size.width+80)];
    view.center=borderView.center;
    
    if (!CGRectContainsRect(self.superview.frame, view.frame)) {
        return;
    }
    ////
    self.transform = CGAffineTransformScale(self.transform, scale, scale);
    borderView.transform = CGAffineTransformScale(borderView.transform, scale, scale);
    imageView.transform = CGAffineTransformScale(imageView.transform, scale, scale);
    pinchRecognizer.scale = 1.0;*/
}
- (void)panDetectedOnController:(UIPanGestureRecognizer *)panRecognizer
{
    CGPoint translation = [panRecognizer translationInView:self.superview];
    panRecognizer.view.center = CGPointMake(panRecognizer.view.center.x + translation.x,
                                         panRecognizer.view.center.y + translation.y);
    [panRecognizer setTranslation:CGPointMake(0, 0) inView:self.superview];
  
    /*
    CGPoint translation = [panRecognizer translationInView:self.superview];
    CGPoint contollerPosition = self.center;
    contollerPosition.x += translation.x;
    contollerPosition.y += translation.y;
    
    CGRect rect=borderView.frame;
    rect.origin.x+=translation.x;
    rect.origin.y+=translation.y;
    self.center = contollerPosition;
    [panRecognizer setTranslation:CGPointZero inView:self];*/
}

-(void)cropImage
{
    CGRect rect = borderView.frame;
    borderView.layer.borderColor=[[UIColor colorWithRed:166.0/255 green:252.0/255 blue:255.0/255 alpha:0.0] CGColor];
    UIImage *image = [self captureView];
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], rect);
      borderView.layer.borderColor=[[UIColor colorWithRed:235.0/255 green:0.0/255 blue:139.0/255 alpha:1.0] CGColor];
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    imageView.image=img;
    imageView.transform = CGAffineTransformIdentity;
    [imageView setFrame:borderView.frame];
}
- (UIImage *)captureView{
    CGRect screenRect = [self bounds];
    UIGraphicsBeginImageContext(screenRect.size);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [[UIColor clearColor] set];
    CGContextFillRect(ctx, screenRect);
    
    [self.layer renderInContext:ctx];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return newImage;
}
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
/*
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint locationPoint = [[touches anyObject] locationInView:self.mainControllerView];
    if (CGRectContainsPoint(borderView.frame,locationPoint)==NO)
    {
        [self deselectController];
    }
}
*/
-(void) deselectController
{
    self.layer.borderColor=[[UIColor colorWithRed:0 green:0 blue:0 alpha:0.0] CGColor];
    borderView.backgroundColor=[UIColor clearColor];
    borderView.layer.borderColor=[[UIColor colorWithRed:166.0/255 green:252.0/255 blue:255.0/255 alpha:0.0] CGColor];
    [self setUserInteractionEnabled:YES];
    [self resignFirstResponder];
    self.isSelected=NO;
    [self removeGesture:YES];
}
-(void)removeGesture:(BOOL)flag{
    if (flag) {
        [self removeGestureRecognizer:dragRecognizer];
        [self removeGestureRecognizer:borderPinchGR];
        [self removeGestureRecognizer:borderRotationGR];
    }
    else{
        [self addGestureRecognizer:dragRecognizer];
        [self addGestureRecognizer:borderPinchGR];
        [self addGestureRecognizer:borderRotationGR];
    }
}
-(void)selectContollerWith:(UIView *)view
{
    view.layer.borderColor=[[UIColor colorWithRed:0 green:0 blue:0 alpha:0.0] CGColor];
    borderView.backgroundColor=[UIColor colorWithWhite:1.0 alpha:0.3];
    borderView.layer.borderColor=[[UIColor colorWithRed:235.0/255 green:0.0/255 blue:139.0/255 alpha:1.0] CGColor];
    [view setUserInteractionEnabled:YES];
    [view becomeFirstResponder];
    self.isSelected=YES;
    [self removeGesture:NO];
}
@end