//
//  ViewClass.h
//  ResizingApp
//
//  Created by  Dulal Hossain on 8/12/12.
//  Copyright (c) 2012 Databiz Software Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
////#import "RotationGestureRecognizer.h"

@interface ViewClass : UIView<UIGestureRecognizerDelegate>
{
    UIView *mainControllerView;
    UIView *borderView;
    UIImageView *imageView;
  
    NSInteger controlXPossition;
    NSInteger controlYPossition;
    
    NSInteger controlWidth;
    NSInteger controlHeight;

  ////  RotationGestureRecognizer *rotation;
  
    UIPanGestureRecognizer *dragRecognizer;
    
    UIPinchGestureRecognizer *borderPinchGR;
    UIRotationGestureRecognizer *borderRotationGR;
}
@property (retain, nonatomic) UIImageView *imageView;
@property (retain, nonatomic) UIImage *image;
@property (retain, nonatomic) UIView *mainControllerView;
@property (retain, nonatomic) UIView *borderView;
////@property (retain, nonatomic) RotationGestureRecognizer *rotation;

-(void)cropImage;
- (UIImage *)captureView:(UIView *)view;

-(void)initController;
-(void)resetContoller;
-(void) deselectController;
-(void) selectContollerWith:(UIView*) view;

-(void)unLockControlView;
-(void)updateBorderCornerButtons;
-(void)updateBorderView;
-(void)adjustCloseAndLockButton;
- (id)initWithFrame:(CGRect)frame controlXPosition:(NSInteger)xPositon controlYPositoin:(NSInteger)yPosition controlWidth1:(NSInteger)width controlHeight1:(NSInteger)height image:(UIImage*)image;
- (void)removeControlView;
@end
