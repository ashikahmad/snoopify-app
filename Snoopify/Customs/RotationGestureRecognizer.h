

#import <UIKit/UIKit.h>


@interface RotationGestureRecognizer : UIGestureRecognizer 
{
    UIButton *rotateButton;
    UIView* otherView;
    CGPoint currentTouchPoint;
    CGPoint previousTouchPoint;
}

/**
 The rotation of the gesture in radians since its last change.
 */
@property (nonatomic, assign) CGPoint currentTouchPoint;
@property (nonatomic, assign) CGPoint previousTouchPoint;

@property (nonatomic, assign) CGFloat rotation;
@property (nonatomic,retain) UIButton *rotateButton;
@property (nonatomic,retain) UIView* otherView;
@end
