//
//  ViewController.h
//  Snoopify
//
//  Created by MAC on 8/27/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VPImageCropperViewController.h"
#import "EditorVC.h"
#define IS_IOS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface HomeVC : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,VPImageCropperDelegate>
@property(nonatomic,strong)IBOutlet UIImageView *bgImageView;
@property(nonatomic,strong)IBOutlet UIImage *originalImage;
-(IBAction)selectPhoto:(id)sender;
-(IBAction)takePhoto:(id)sender;
-(IBAction)shareButtonAction:(id)sender;
-(IBAction)nRButtonAction:(id)sender;
-(IBAction)snopifyLogoButtonAction:(id)sender;
-(IBAction)twitterFollowButtonAction:(id)sender;
-(IBAction)instagramFollowButtonAction:(id)sender;
@end

